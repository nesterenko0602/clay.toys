const fs = require('fs');
const restify = require('restify');
const MongoClient = require('mongodb').MongoClient;
const sendmail = require('sendmail')();
const tableify = require('tableify');

/**
 * Mongo
 */
const mongoUrl = 'mongodb://localhost:27017';
let client;

MongoClient.connect(mongoUrl).then((clientObj) => {
  client = clientObj;
});

/**
 * Server
 */
const httpsOptions = {
  key: fs.readFileSync('./cert/private.key'),
  certificate: fs.readFileSync('./cert/certificate.crt'),
};
const server = restify.createServer(httpsOptions);

server.use(restify.plugins.bodyParser({
  mapParams: true,
}));

server.listen(8127, () => {
  console.log('%s listening at %s', server.name, server.url);
});

function unknownMethodHandler(req, res) {
  if (req.method.toLowerCase() !== 'options') {
    return res.send(new restify.MethodNotAllowedError());
  }

  const allowHeaders = ['Accept', 'Content-Type'];

  if (res.methods.indexOf('OPTIONS') === -1) res.methods.push('OPTIONS');

  res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
  res.header('Access-Control-Allow-Methods', res.methods.join(', '));
  res.header('Access-Control-Allow-Origin', '*');

  return res.send(200);
}

server.on('MethodNotAllowed', unknownMethodHandler);

/**
 * Access points
 */
server.post('/saveOrder', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');

  client.db('clay').collection('orders').insertOne(req.body);

  res.send({ status: true, order: req.body });

  const { contacts: { name }, total } = req.body;

  sendmail(
    {
      from: 'noreply@clay.toys',
      to: 'nesterenko0602@gmail.com',
      subject: `New Order by ${name} for ${total} euros`,
      html: tableify(req.body),
    },
    (err, reply) => {
      console.log(err && err.stack);
      console.dir(reply);
    }
  );

  return next();
});

/**
 * Test method
 */
server.get('/test', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');

  res.send('its ok');

  return next();
});

server.on('close', () => {
  client.close();
});
