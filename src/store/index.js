import Vue from 'vue';
import Vuex from 'vuex';

import cart from './modules/cart';
import checkout from './modules/checkout';
import items from './modules/items';
import ui from './modules/ui';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    cart,
    checkout,
    items,
    ui,
  },
});
