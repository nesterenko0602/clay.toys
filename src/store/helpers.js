/**
 * Save every changes of state in localStorage
 *
 * @param {Object} initialState
 * @param {string} name Name of state field
 */
export const applyProxy = (initialState, name) => new Proxy(initialState, {
  set: (obj, prop, value) => {
    obj[prop] = value; // eslint-disable-line

    if (window.localStorage) {
      window.localStorage.setItem(name, JSON.stringify(obj));
    }

    return true;
  },
});

/**
 *
 * @param {string} name Name of state field
 * @param {Object} initialState
 * @param {Array<string>} excludedFields Not restored fields
 */
export const restoreState = (name, initialState, excludedFields = []) => {
  if (!window.localStorage) {
    return initialState;
  }

  const savedValue = JSON.parse(localStorage.getItem(name));

  if (!savedValue) {
    return initialState;
  }

  if (
    typeof savedValue !== 'object' ||
    Object.keys(savedValue).length !== Object.keys(initialState).length
  ) {
    return initialState;
  }

  const filteredValue = Object.keys(savedValue)
    .filter(key => excludedFields.indexOf(key) < 0)
    .reduce((newObj, key) => Object.assign(newObj, { [key]: savedValue[key] }), {});

  return {
    ...initialState,
    ...filteredValue,
  };
};
