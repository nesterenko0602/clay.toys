/**
 * State
 */
const state = {
  presets: [
    {
      term: 'fruits_set',
      title: 'Fruits set',
      items: [
        'apple',
        'apricot',
        'avocado',
        'banana',
        'lemon',
        'orange',
        'peach',
        'pear',
        'plum',
        'pomegranate',
        'watermelon',
      ],
    },
    {
      term: 'vegies_set',
      title: 'Vegies set',
      items: [
        'beetroot',
        'broccoli',
        'carrot',
        'chili_pepper',
        'cucumber',
        'eggplant',
        'mushroom',
        'potato',
        'pumpkin',
        'squash',
        'tomato',
      ],
    },
    {
      term: 'mix_set',
      title: 'Mix set',
      items: [
        'bun',
        'cheese',
        'cinnamon_bun',
        'cookie',
        'donut',
        'dumpling',
        'fried_egg',
        'icecream',
        'loaf',
        'macaroni',
        'rye_bread',
      ],
    },
  ],
  items: [
    {
      term: 'apple',
      title: 'Apple',
    },
    {
      term: 'apricot',
      title: 'Apricot',
    },
    {
      term: 'avocado',
      title: 'Avocado',
    },
    {
      term: 'banana',
      title: 'Banana',
    },
    {
      term: 'beetroot',
      title: 'Beetroot',
    },
    {
      term: 'watermelon',
      title: 'Watermelon',
    },
    {
      term: 'broccoli',
      title: 'Broccoli',
    },
    {
      term: 'bun',
      title: 'Bun',
    },
    {
      term: 'carrot',
      title: 'Carrot',
    },
    {
      term: 'cheese',
      title: 'Cheese',
    },
    {
      term: 'chili_pepper',
      title: 'Chili',
    },
    {
      term: 'cinnamon_bun',
      title: 'Cinnamon Bun',
    },
    {
      term: 'cookie',
      title: 'Cookie',
    },
    {
      term: 'cucumber',
      title: 'Cucumber',
    },
    {
      term: 'donut',
      title: 'Donut',
    },
    {
      term: 'dumpling',
      title: 'Dumpling',
    },
    {
      term: 'eggplant',
      title: 'Eggplant',
    },
    {
      term: 'fried_egg',
      title: 'Fried Egg',
    },
    {
      term: 'icecream',
      title: 'Icecream',
    },
    {
      term: 'lemon',
      title: 'Lemon',
    },
    {
      term: 'loaf',
      title: 'Loaf',
    },
    {
      term: 'macaroni',
      title: 'Macaroni',
    },
    {
      term: 'mushroom',
      title: 'Mushroom',
    },
    {
      term: 'orange',
      title: 'Orange',
    },
    {
      term: 'peach',
      title: 'Peach',
    },
    {
      term: 'pear',
      title: 'Pear',
    },
    {
      term: 'plum',
      title: 'Plum',
    },
    {
      term: 'pomegranate',
      title: 'Garnet',
    },
    {
      term: 'potato',
      title: 'Potato',
    },
    {
      term: 'pumpkin',
      title: 'Pumpkin',
    },
    {
      term: 'rye_bread',
      title: 'Rye Bread',
    },
    {
      term: 'squash',
      title: 'Squash',
    },
    {
      term: 'tomato',
      title: 'Tomato',
    },
  ],
};

/**
 * Getters
 */
const getters = {
  getPresets: currentState => currentState.presets,
  getItems: currentState => currentState.items.sort((a, b) => {
    if (a.title < b.title) return -1;
    if (a.title > b.title) return 1;

    return 0;
  }),
};

/**
 * Actions
 */
const actions = {
};

/**
 * Mutations
 */
const mutations = {
};

export default {
  state,
  getters,
  actions,
  mutations,
};
