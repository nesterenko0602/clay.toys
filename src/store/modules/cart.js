import { applyProxy, restoreState } from '../helpers';

/**
 * State
 */
const initialState = restoreState('cart', { added: [] });
const state = applyProxy(initialState, 'cart');

/**
 * Getters
 */
const getters = {
  getCartList: currentState => currentState.added.filter(item => item.quantity),

  getCartTotalAmounts: (currentState, rootGetters) => {
    const products = rootGetters.getQuantityTable;

    return Object.keys(products).reduce((res, key) => {
      const isSet = key.indexOf('_set') !== -1;
      const typeOfItem = isSet ? 'sets' : 'items';
      const newValue = res[typeOfItem] + products[key];

      return {
        ...res,
        [typeOfItem]: newValue,
      };
    }, { items: 0, sets: 0 });
  },

  getIsCartEmpty: (currentState, rootGetters) => !rootGetters.getCartList.length,

  getTotalPrice: (currentState, rootGetters) => {
    const amounts = rootGetters.getCartTotalAmounts;

    return parseFloat(((amounts.items * 0.8) + (amounts.sets * 7)).toFixed(1));
  },

  getQuantityTable: currentState => currentState.added.reduce((result, item) => {
    result[item.term] = item.quantity; // eslint-disable-line

    return result;
  }, {}),
};

/**
 * Actions
 */
const actions = {
  addToCart({ state: currentState, commit }, { term }) {
    const cartItem = currentState.added.find(item => term === item.term);
    if (!cartItem) {
      commit('addToCart', term);
    } else {
      commit('incrementItemQuantity', cartItem);
    }
  },

  dropCart({ commit }) {
    commit('dropCart');
  },

  takeFromCart({ state: currentState, commit }, { term }) {
    const cartItem = currentState.added.find(item => term === item.term);
    if (cartItem.quantity <= 1) {
      commit('removeFromCart', term);
    } else {
      commit('decrementItemQuantity', cartItem);
    }
  },

  removeFromCart({ commit }, { term }) {
    commit('removeFromCart', term);
  },
};

/**
 * Mutations
 */
const mutations = {
  addToCart(currentState, term) {
    currentState.added = [ // eslint-disable-line
      ...currentState.added,
      {
        term,
        quantity: 1,
      },
    ];
  },

  decrementItemQuantity(currentState, { term, quantity }) {
    currentState.added = [
      ...currentState.added.map((item) => {
        const newQuantity = term === item.term
          ? quantity - 1
          : item.quantity;

        return {
          ...item,
          quantity: newQuantity,
        };
      }),
    ];
  },

  dropCart(currentState) {
    currentState.added = [];
  },

  incrementItemQuantity(currentState, { term, quantity }) {
    currentState.added = [
      ...currentState.added.map((item) => {
        const newQuantity = term === item.term
          ? quantity + 1
          : item.quantity;

        return {
          ...item,
          quantity: newQuantity,
        };
      }),
    ];
  },

  removeFromCart(currentState, term) {
    currentState.added = currentState.added.filter(item => term !== item.term);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
