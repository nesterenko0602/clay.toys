import { applyProxy, restoreState } from '../helpers';

const host = `${location.origin}:8127`;

/**
 * Values
 */
const packagingTypes = [
  {
    term: 'simple',
    price: 0,
  }, {
    term: 'solid',
    price: 1,
  },
];

const deliveryAddresses = [
  {
    term: 'home',
    address: 'Vasileiou Kousouli, 5',
    hours: 'Everyday after 7&nbsp;P.M.',
    marker: {
      position: {
        lat: 34.677249,
        lng: 33.031649,
      },
      label: '1',
    },
    link: 'https://goo.gl/maps/zTbo8hQRY8p',
  },
  {
    term: 'work',
    address: 'Arch. Makarios III Avenue, 124',
    hours: 'At working days from 10&nbsp;A.M. to 6&nbsp;P.M.',
    marker: {
      position: {
        lat: 34.685317,
        lng: 33.033078,
      },
      label: '2',
    },
    link: 'https://goo.gl/maps/Bn44NWNV7KP2',
  },
];

const paymentMethods = [
  {
    term: 'cash',
    title: 'Cash at pick-up point',
  },
  {
    term: 'boc',
    title: 'Transfer to Bank of Cyprus account',
  },
  {
    term: 'revolut',
    title: 'Transfer to Revolut account',
  },
];

/**
 * State
 */
const initialState = {
  checkoutStatus: 0,
  packagingType: 'simple',
  address: 'home',
  paymentMethod: 'cash',
  contacts: {
    name: '',
    phone: '',
    childName: '',
    comments: '',
  },
};

const store = restoreState(
  'checkout',
  initialState,
  ['checkoutStatus'],
);

const state = applyProxy(store, 'checkout');

/**
 * Getters
 */
const getters = {
  getOrder: (currentState, globalGetters) => {
    const { checkoutStatus, ...rest } = currentState;

    return {
      ...rest,
      items: globalGetters.getCartList,
      total: globalGetters.getTotalCheckoutPrice,
    };
  },
  getCheckoutStatus: currentState => currentState.checkoutStatus,
  getTotalCheckoutPrice: (currentState, globalGetters) => (
    globalGetters.getTotalPrice + Number(globalGetters.getPackagingType !== 'simple')
  ),

  getAddresses: () => deliveryAddresses,
  getAddress: currentState => currentState.address,

  getComments: currentState => currentState.contacts.comments,
  getChildName: currentState => currentState.contacts.childName,
  getName: currentState => currentState.contacts.name,
  getPhone: currentState => currentState.contacts.phone,

  getPackages: () => packagingTypes,
  getPackagingType: currentState => currentState.packagingType,

  getPaymentMethod: currentState =>
    paymentMethods.find(item => item.term === currentState.paymentMethod),
  getPaymentMethods: () => paymentMethods,
};

/**
 * Actions
 */
const actions = {
  chooseAddress({ commit }, term) {
    commit('chooseAddress', term);
  },

  choosePackaging({ commit }, { term }) {
    commit('choosePackaging', term);
  },

  choosePaymentMethod({ commit }, term) {
    commit('choosePaymentMethod', term);
  },

  completeOrder({ dispatch, getters: globalGetters }) {
    fetch(`${host}/saveOrder`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(globalGetters.getOrder),
    })
      .then((response) => {
        if (response.status !== 200) {
          throw Error();
        }

        dispatch('proceedCheckout');
        dispatch('dropCart');
      })
      .catch(() => {
        alert('Something went wrong. We have saved your order, try later please.');
      });
  },

  goToCheckoutStage({ state: currentState, commit }, newStage) {
    if (currentState.checkoutStatus > newStage) {
      commit('goToCheckoutStage', newStage);
    }
  },

  proceedCheckout({ commit, dispatch, state: currentState }) {
    if (currentState.checkoutStatus === 6) {
      dispatch('toggleCartVisibility', false);
      commit('goToCheckoutStage', 0);

      return;
    }

    commit('proceedCheckout');
  },
};

/**
 * Mutations
 */
const mutations = {
  chooseAddress(currentState, address) {
    currentState.address = address;
  },

  choosePackaging(currentState, packaging) {
    currentState.packagingType = packaging;
  },

  choosePaymentMethod(currentState, paymentMethod) {
    currentState.paymentMethod = paymentMethod;
  },

  goToCheckoutStage(currentState, newStage) {
    currentState.checkoutStatus = newStage;
  },

  proceedCheckout(currentState) {
    currentState.checkoutStatus += 1;
  },

  updateMessage(currentState, data) {
    currentState.contacts[data.name] = data.value;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
