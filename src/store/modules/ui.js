/**
 * State
 */
const state = {
  isCartShown: false,
};

/**
 * Getters
 */
const getters = {
  getIsCartShown: currentState => currentState.isCartShown,
};

/**
 * Actions
 */
const actions = {
  toggleCartVisibility ({ state: currentState, commit }, isExpanded) {
    const shouldBeShown = (
      isExpanded === true
      || !currentState.isCartShown
    );

    commit('toggleCartVisibility', shouldBeShown);
  },
};

/**
 * Mutations
 */
const mutations = {
  toggleCartVisibility (currentState, newState) {
    currentState.isCartShown = newState;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
